# MonitorDirectory

Monitor a directory for incoming zip files. Call a command (initating tranSMART-batch), when a new file is dected.

Four subdirectories will be created at the choosen source directory:
* incoming (put new .rar files in here to auto upload them into tranSMART)
* archive (all detected files will be put here with data as well as SUCCESS and FAILED stamps)
* temp (here is all the acutal upload conducted)
* log (all log files will appear here)

## Execute  

Edit context in MonitorDirectory/tos/monitordirectory_0_1/contexts. Configure MonitorDirectory/MonitorDirectory_run.sh with choosen context and run:

```
./MonitorDirectory/MonitorDirectory_run.sh > MonitorDirectory.log &
```

## Edit with Talend Open Studio

Import Items from the TOS/process folder to edit the Job in Talend Open Studio for Data Integration in version 7.0.1 or higher.